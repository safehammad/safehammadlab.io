---
title: Is English Tonal?
date: '2013-03-31'
aliases: [/2013/03/31/is-english-tonal/]
categories:
  - Natural Language Processing
  - Software
tags:
  - Python
---

An interesting feature of several widely used Asian languages is that they're tonal. In tonal languages, changing the intonation of what seems to be the same word (at least to the Western ear) can markedly change the meaning of that word. This can be quite hard to fathom for the typical English speaker. A celebrated example of this can be found in Mandarin Chinese:

妈 mā mother<br/>
麻 má hemp<br/>
马 mǎ horse<br/>
骂 mà scold<br/>
吗 ma (question tag)

I was in an English supermarket recently and read the word "discount" on several items for sale. It occurred to me that the word discount can be used with at least a couple of different but related meanings:

In the supermarket it's often used as a noun meaning "a reduction in the sale price".
It can also mean the verb "to dismiss", "to remove from consideration" and sometimes "to reduce in price".
What then struck me is that these two usages are spelled the same but pronounced differently. In the first meaning, the first syllable is stressed whereas in the second meaning, the second syllable is stressed. I tried to think of more words which followed this pattern and it took me some time to come up with "reject", "survey" and "upset". My hunch was that there were plenty more words like that so I set about seeing if I could automate finding them.

One can argue that changing the stresses on a word's syllables changes its intonation. Does that make English tonal after all, albeit on a small scale?

## Pronunciation

The [Carnegie Mellon Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) is a machine-readable pronunciation dictionary for North American English. Its database of 100,000+ words contains a set of pronunciations organised as a list of sounds, for example:

```
tree = ['T', 'R', 'IY1']
biscuit = ['B', 'IH1', 'S', 'K', 'AH0', 'T']
undo = ['AH0', 'N', 'D', 'UW1']
```

I'm interested here not in the actual consonant and vowel sounds which can vary quite markedly with differences in regional accent, but in the stresses of the vowel sounds. These are indicated by a numeric suffix:

0 - No stress<br/>
1 - Primary stress<br/>
2 - Secondary stress<br/>

In the examples above, "biscuit" is pronounced with the stress on the first syllable and "undo" with the stress on the second.

In the Python programming language, the CMU Pronouncing Dictionary can be accessed using the [Natural Language Toolkit](http://nltk.org/) (NLTK). If you're using the NLTK for the first time, you'll need to do the following:

```python
>>> import nltk
>>> nltk.download()
```

A GUI will appear where you can choose to download the CMU Pronouncing Dictionary. This only needs to be done once. The dictionary can then be accessed as follows:

```python
>>> from nltk.corpus import cmudict
>>> pronunciations = cmudict.dict()
>>> pronunciations['tree']
[['T', 'R', 'IY1']]
>>> pronunciations['discount']
[['D', 'IH0', 'S', 'K', 'AW1', 'N', 'T'], ['D', 'IH1', 'S', 'K', 'AW0', 'N', 'T']]
```

Here we can see that "discount" is indeed listed with more than one pronunciation. Now lets distil the stresses in these pronunciations:

```python
>>> def stresses(pronunciation):
...     return [i[-1] for i in pronunciation if i[-1].isdigit()]
...
>>> stresses(['D', 'IH0', 'S', 'K', 'AW1', 'N', 'T'])
['0', '1']
>>> stresses(['D', 'IH1', 'S', 'K', 'AW0', 'N', 'T'])
['1', '0']
```

So in one pronunciation, the stress is on the first syllable and in the other pronunciation, the stress is on the second, just as we suspected.

## Part of Speech

[WordNet](http://wordnet.princeton.edu/) is a lexical database of English nouns, verbs, adjectives and adverbs. The database lists the multiple uses of a given word, and for any given use, its definition and most remarkably, its relationship to other words. For example, "dog" is a type of "canine" and a "poodle" is a type of "dog". We're interested in the fact that WordNet also helpfully stores the part of speech (i.e. noun, verb etc.) for any given usage.

WordNet can also be accessed using NLTK. Once again, for first use, the WordNet database needs to be downloaded using nltk.download().

Each usage of a word is called a "synset" (i.e. Synonym Set) in WordNet parlance and can be accessed as follows:

```python
>>> from nltk.corpus import wordnet
>>> wordnet.synsets('discount')
[Synset('discount.n.01'), Synset('discount_rate.n.02'), Synset('rebate.n.01'), Synset('deduction.n.02'), Synset('dismiss.v.01'), Synset('discount.v.02')]
```

As might be apparent from this example, the synset's primary word may or may not be 'discount'. In fact, each synset contains a list of words (known as lemmas) which can represent that usage:

```python
>>> synsets = wordnet.synsets('discount')
>>> synsets[0]
Synset('discount.n.01')
>>> synsets[0].definition
'the act of reducing the selling price of merchandise'
>>> synsets[0].lemma_names
['discount', 'price_reduction', 'deduction']
```

We'll concentrate on those synsets whose primary lemma is the word we are interested in.

Finally, the part of speech for a synset is easily obtained:

```python
>>> synsets[0]
Synset('discount.n.01')
>>> synsets[0].definition
'the act of reducing the selling price of merchandise'
>>> synsets[0].pos
'n'
>>> synsets[5]
Synset('discount.v.02')
>>> synsets[5].definition
'give a reduction in price on'
>>> synsets[5].pos
'v'
```

## Putting It All Together

So to find our "tonal" words, all we need to do is find words which fit the following criteria:

1. Two or more syllables.
1. Multiple pronunciations with different stresses.
1. Can be used as a noun or verb.

A sample Python script can be found [here](https://gist.github.com/safehammad/5276713).

And here's the full list of 112 tonal English words found using this script:

`['addict', 'address', 'affiliate', 'affix', 'ally', 'annex', 'associate', 'average', 'bachelor', 'buffet', 'combine', 'commune', 'compact', 'compound', 'compress', 'concert', 'concrete', 'confederate', 'conflict', 'content', 'contest', 'contract', 'contrast', 'converse', 'convert', 'convict', 'coordinate', 'correlate', 'costume', 'debut', 'decrease', 'defect', 'delegate', 'desert', 'detail', 'detour', 'dictate', 'digest', 'discharge', 'discount', 'duplicate', 'effect', 'escort', 'estimate', 'excerpt', 'excise', 'ferment', 'finance', 'forearm', 'geminate', 'general', 'graduate', 'impact', 'implant', 'import', 'impress', 'imprint', 'increase', 'insert', 'interest', 'intrigue', 'invalid', 'laminate', 'leverage', 'mentor', 'mismatch', 'object', 'offset', 'overflow', 'permit', 'pervert', 'postulate', 'predicate', 'present', 'privilege', 'produce', 'progress', 'project', 'protest', 'ratchet', 'recall', 'recess', 'record', 'recount', 'reference', 'refund', 'regress', 'research', 'reset', 'retake', 'rewrite', 'romance', 'segment', 'separate', 'sophisticate', 'subject', 'submarine', 'subordinate', 'supplement', 'surcharge', 'survey', 'suspect', 'syndicate', 'syringe', 'transfer', 'transport', 'trespass', 'underestimate', 'update', 'upgrade', 'upset', 'veto']`

## Observations

Interesting observations include:

1. In most cases, stressing the first syllable yields the noun whereas stressing a later syllable yieds the verb.
1. The noun and verb are usually closely related in meaning, however the nouns of some words have taken on a common usage which has detached it from the meaning of the verb. Obvious examples include "project", "subject"... and "pervert"!
1. There also seems to be a high frequency of words beginning with 'com', 'con' and 're'. Is this significant or is this is common of English verbs? I'll leave that question as an exercise for the reader.

With a minor tweak to the script, we can find words that are combinations of adjectives, nouns and verbs. This gives us much smaller lists of words:

* adjective/noun: `['antecedent', 'commemorative', 'compact', 'complex', 'compound', 'concrete', 'deliverable', 'eccentric', 'general', 'hostile', 'inside', 'invalid', 'invertebrate', 'juvenile', 'liberal', 'mineral', 'national', 'natural', 'oblate', 'peripheral', 'present', 'salient', 'separate', 'subordinate', 'worsening']`
* adjective/verb: `['abstract', 'alternate', 'animate', 'appropriate', 'articulate', 'compact', 'compound', 'concrete', 'frequent', 'general', 'invalid', 'moderate', 'perfect', 'present', 'separate', 'subordinate']`
* adjective/noun/verb: `['compact', 'compound', 'concrete', 'general', 'invalid', 'present', 'separate', 'subordinate']`

## Epilogue

It turns out that what we've found here are [heteronyms](http://en.wikipedia.org/wiki/Heteronym_(linguistics)) which are two or more words which share the same spelling (also known as [homographs](http://en.wikipedia.org/wiki/Homograph)) but have different meanings. More specifically, we've found plenty of [initial-stress-derived nouns](http://en.wikipedia.org/wiki/Initial-stress-derived_noun) where a verb can be turned into a noun by stressing the first syllable.

I'm not sure we've proven that English is a truly tonal language, but this has been a good exercise in cross-referencing two major natural language databases to find interesting words.
