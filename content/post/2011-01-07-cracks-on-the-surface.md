---
title: Cracks on the Surface
date: '2011-01-07'
aliases: [/2011/01/07/cracks-on-the-surface/]
categories:
  - Random
tags:
  - Natural World
---

The recent freeze and thaw of the canal beside where I live has produced a beautiful natural phenomenon. Cracks in the surface ice reminiscent of neurons with pronounced dendrites have appeared in random locations. Stars in the night sky also spring to mind.

![](/images/ice-neurons.jpg "Neurons")
![](/images/ice-dendrites.jpg "Dendrites")
