---
title: Be Good to your Colon (in your Python Code)
date: '2011-08-10'
aliases: [/2011/08/10/be-good-to-your-colon/]
categories:
  - Python
---

Programmers spend more time reading code than writing it (a fact well known by most programmers who tend not to publicise this to their employers).  It therefore stands to reason that (most?) programming languages should be designed as much for human consumption as for machine consumption and should be as readable as possible.

Python is a very readable language (a fact which contributes to its popularity) and has been termed “executable pseudocode” on account of its readability.  An aspect of Python which makes it readable is its avoidance of syntactic fluff, extraneous words and symbols which add nothing to the code’s meaning but serve to detract from it.

In the past I’ve felt somewhat negative about Python’s terminal colon “:”, the symbol used to terminate `if`, `while`, `def` and `class` statements and to signify the start of a new block of indented code.  For example:

```python
def do_something_cool():
    return 'Doing something cool'

if a == 1:
    b = do_something_cool()
```

Even without the colon, it’s quite clear that we’re starting a new block of indented code because (a) the statement starts with the keyword `if`, `while`, `def` or `class` and (b) the next line of code is indented. For comparison, [Ruby](http://www.ruby-lang.org/en/) gets on just fine without the colon after its def statement. So why the need for a colon in Python? Is it syntactic fluff?

The [Python FAQ](http://docs.python.org/faq/design.html#why-are-colons-required-for-the-if-while-def-class-statements) explains that the colon enhances readability and helps editors with syntax highlighting and code indentation. Lets face it, any self respecting editor should be capable of parsing a line beginning with an `if`, `while`, `def` or `class`, so the “helps editors” argument is bogus. I do however buy the argument that the code is visibly more readable. But how does it enhance readability?

I’ve already mentioned that a programmer spends more time reading than writing code. What I haven’t yet suggested is that a programmer will often reread and scan the same code repeatedly to form a mental picture of a larger codebase. It’s what the eyes do when they’re scanning code that’s key to the importance of the colon. There is [some evidence](http://www.ncbi.nlm.nih.gov/pubmed/20373225) to suggest that the eyes linger at the beginning and at the end of a sentence when reading text and draw especially from visual cues at those locations. Let’s assume for the moment that this holds true for a line of code. So the visual cue heralding an indented block of code is clear at the beginning of a line of code, namely an `if`, `while`, `def` or `class` followed by an indented line. The only visual cue at the end of a line of Python code is the colon, and without the colon there would be no cue. So even though the colon is not strictly necessary, there is an argument that its existence is there for human consumption and aids readability.

When all’s said and done, the advantage of the colon is probably slight at best, and then probably only for a newcomer to the language. (This sort of advantage possibly completely vanishes for experienced users of any language). Never-the-less, on balance, I’m now happy it’s there!


