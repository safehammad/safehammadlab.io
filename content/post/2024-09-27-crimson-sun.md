---
title: Crimson Sun - Generative Art No. 1
date: '2024-09-27'
categories:
  - Generative Art
  - Software
tags:
  - Clojure
---

![](/images/crimson-sun.png "Crimson Sun")

*Crimson Sun* is the first piece of artwork in my [Generative Art Practice]({{< relref "2024-09-20-generative-art-practice.md" >}}) series. The initial seed of the idea was a sunflower rather than a sun. I had planned to create a circle and fill it with triangles which would represent the sunflower's seeds and see where that took me, but very quickly, especially as I started to colour the output in reddish tones, I realised that it looked less like a sunflower and more like a sun :)

An interesting aspect of this work is the not-so-random placement and dimensions of the numerous triangles. A common strategy for this would be to use random number generation, however I wanted the placement to be much more homogeneous. Enter the [Halton Sequence](https://en.wikipedia.org/wiki/Halton_sequence), a deterministic sequence which appears random for most purposes, however the generated numbers are well distributed as they progress.

The Halton Sequence requires seeding with an integer "base" from which to start. Each base will result in a different sequence, much like seeding a random number generator. For example, base 2 results in the following sequence:

```
1⁄2,
1⁄4, 3⁄4,
1⁄8, 5⁄8, 3⁄8, 7⁄8,
1⁄16, 9⁄16, 5/16, 13/16, 3/16, 11/16, ...
```

And base 3 results in the following:

```
1/3, 2/3,
1/9, 4/9, 7/9, 2/9, 5/9, 8/9,
1/27, 10/27, 19/27, 4/27, 13/27, 22/27, 7/27, ...
```

Hopefully this gives a little insight into the algorithm without looking at the code, but if you'd like to look at the code, the file [utils.clj](https://github.com/safehammad/generative-art-practice/blob/main/src/com/safehammad/generative_art_practice/utils.clj) contains utilities for generating a Halton sequence for any given base. In addition, it contains a function for creating groups of numbers, each with a given base, useful for generating, for example, coordinates and triangle sizes which require several numbers at a time.

Code used to create all artwork in the Generative Art Practice series can be found in this [Github repository](https://github.com/safehammad/generative-art-practice).
