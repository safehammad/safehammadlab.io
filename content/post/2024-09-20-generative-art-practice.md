---
title: Generative Art Practice
date: '2024-09-20'
categories:
  - Generative Art
  - Software
tags:
  - Clojure
---

![](/images/koch-small.png "Koch Snowflake")

In September 2024 I attended the amazing [Heart of Clojure](https://2024.heartofclojure.eu/) conference. [Lu Wilson](https://www.todepond.com/) gave an inspiring [keynote talk](https://2024.heartofclojure.eu/talks/what-it-means-to-be-open/) about the rewards of an "open practice" of sharing openly, early and continually, no matter how scrappy the work. That talk inspired me to start a schedule of generative art practice to create a regular output of generative art.

Every couple of weeks I plan to post my latest artistic creation to this blog for all to criticise, and maybe enjoy. Rather than creating anything too polished and therefore time consuming, my aim is to create interesting and varied art in a reasonable amount of time and of reasonable quality.

[Generative art](https://en.wikipedia.org/wiki/Generative_art) is a term often used to mean independently generated art, wholly or partly generated automatically via pre-determined rules, in my case, by a computer program. I generally code my studies in Clojure leaning on the excellent [Quil](https://github.com/quil/quil) library, which in turn is based on the celebrated [Processing](https://processing.org/) visual arts software, to provide the drawing routines. Although Quil is capable of creating animations, I plan to focused on creating static drawings, the type which can be printed and hung on the wall for all to see :)

In the past I've tended to create highly geometric pieces, not dissimilar to the [Koch Snowflake](https://en.wikipedia.org/wiki/Koch_snowflake) at the top of this post. My hope with this practice is to create what I might describe as more "natural" or "organic" looking pieces, something which has so far eluded me.

Code used to create the artwork can be found in this [Github repository](https://github.com/safehammad/generative-art-studies).
