---
title: Pro Python - Book Review
date: '2011-03-14'
aliases: [/2011/03/14/pro-python-book-review/]
categories:
  - Python
  - Book Reviews
tags:
  - Python
---

![](/images/pro-python-book.jpg#fig-right "Front cover")

A recent thread on the [Python Northwest](http://pynw.org.uk/) mailing list asked for opinions on [Marty Alchin](http://martyalchin.com/)‘s book, [Pro Python](https://www.apress.com/gp/book/9781430227571). I thought I’d reproduce the answer I gave and expand on it a little.

I’ve owned Marty Alchin’s first book, Pro Django, for some time and was very happy with that purchase.  Based on that, I decided to buy his Pro Python book last year.  Pro Python is targeted at readers who are proficient with basic Python but are looking to push their skills further.  Quite naturally there’s a large number of beginners’ Python books out there but a shortage of more advanced books so it was nice to see this published.

Marty Alchin starts his book with a refreshing approach.  Rather than regurgitating Python facts to the reader, he takes a step by step tour of [The Zen of Python](http://www.python.org/dev/peps/pep-0020/) discussing how it’s philosophy can be practically applied to make your programming more Pythonic.  He then delves into traditional topics such as classes, objects and strings as well as development topics such as packaging and testing.

I like Marty Alchin’s style of writing and find it to be clear and concise.  Even if you’re reasonably knowledgeable about the advanced topics he covers such as metaclasses, descriptors, introspection and multiple inheritance, I think the book benefits from the fact that these topics are backed up with good examples of how they work, and just as importantly, how they might usefully be used in ways you might not have seen before.  In fact, Chapter 11 walks through the building of a real world Python library which can be found on PyPI (try pip install Sheets) using the principles outlined in the previous chapters.

The other aspect of the book I find very useful is the fact that it is based on Python 3, however all examples are annotated and compared with the “legacy” Python 2 equivalent where relevant.  I’ve gotten a lot more comfortable with Python 3 by reading this book and better understand the improvements in the language from Python 2 to Python 3.

This isn’t a book aimed at newcomers to Python, even if you have a lot of programming experience, as it expects a reasonable amount of basic Python proficiency.  It’s also a “thin” book in the sense that it gives each topic a light treatment rather than aiming to be a complete reference.  This may or may not suit your needs, however there’s plenty of reference material elsewhere both online (e.g. the official Python documentation) and in print.

By comparison, the other advanced Python book I’ve read (and reread!) is [Python In a Nutshell](https://www.oreilly.com/library/view/python-in-a/0596100469/) by Alex Martelli.  It’s based on Python 2.5 and getting a bit out of date, but much of it is still very relevant for all Python 2.x versions.  (I think a Python 3 version might be in the works).  It’s a much heftier and more detailed book and acts as much a reference text as well as being a book you’d enjoy reading from cover to cover.

In summary, I’d recommend Pro Python to any intermediate level Python programmer who’d like to advance their Python skills with a clear and concise text.

_N.B. I am in no way associated with Pro Python, Apress or Marty Alchin … except of course for owning the book!_
