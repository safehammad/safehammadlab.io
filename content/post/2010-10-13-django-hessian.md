---
title: Django-Hessian
date: '2010-10-13'
aliases: [/software/django-hessian/]
categories:
  - Python
tags:
  - Hessian
  - RPC
---

# About

The django-hessian library serves objects via Django using the Hessian RPC protocol.

# Requirements

The django-hessian library requires a version of the mustaine Python Hessian library >=0.1.3 which includes a Hessian WSGI server implementation.

See http://github.com/safehammad/mustaine.

# Usage

Objects can be served using a `djangohessian.Dispatcher` at a given URL with an entry `urls.py`.  The following example serves an object of type `server.Calculator` at the URL http://localhost:8000/rpc/calculator/ in the Django development server:


```python
# mysite/myapp/server.py
from djangohessian import exposed

class Calculator(object):
    @exposed
    def add(self, a, b):
        return a + b

    @exposed
    def subtract(self, a, b):
        return a - b
```

```python
# mysite/urls.py:
from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^rpc/', include('mysite.myapp.urls')),
)
```

```python
# mysite/myapp/urls.py:
from django.conf.urls.defaults import *
from djangohessian import Dispatcher

from server import Calculator

urlpatterns = patterns('',
   url(r'^calculator/', Dispatcher(Calculator())),
)
```

Note that any methods you wish to expose on the served object must be decorated with `@exposed`.

The following is a sample client interactive session written using the mustaine Hessian library:

```python
>>> from mustaine.client import HessianProxy
>>> h = HessianProxy('http://localhost:8000/rpc/calculator/')
>>> h.add(2, 3)
5
>>> h.subtract(7, 3)
4
```
Full source can be found at http://bitbucket.com/safehammad/django-hessian/.
