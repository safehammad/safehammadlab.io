---
title: Our Days are Numbered
date: '2011-10-28'
aliases: [/2011/10/28/our-days-are-numbered/]
categories:
  - Language
---

Whenever I learn a new word in any language, I often find myself comparing that word with equivalent words in other languages. I was recently thinking about words for days of the week in various languages. For related languages, not only are there similarities in the words themselves, but the origins of those words also fall into a small number of distinct categories.

I chose three groups of related languages, partly for reasons of familiarity and interest, and partly to allow me to compare words within and across groups:

* English and German (Germanic)
* French and Spanish (Romance)
* Arabic and Hebrew (Semitic)

Interestingly, the origins of the words for days of the week can almost all be placed into the categories _Planetary_, _Pagan Gods_, _Religious_ and _Numeric_. The following table lists the words for days of the week in each language together with their meaning or origin:

[![](/images/days-of-week.png "The origins of words for days of the week")](/images/days-of-week.png)

What struck me immediately is that this table is strongly reminiscent of the [Periodic Table](http://wikipedia.org/wiki/Periodic_table). This probably isn’t surprising considering that related languages have been placed adjacent to each other.

This [Wikipedia page](http://wikipedia.org/wiki/Weekday_names) provides a more complete study of the origins of the words for days of the week (but without the colourful table).

## Update

Mercury, Venus, Mars, Jupiter and Saturn have been known about since ancient times and were named after Roman gods. So many of the planetary days of the week were actually named after ancient gods, albeit indirectly. Thanks to M Stallman for pointing this out!

Perhaps it makes sense that the sun and moon being the most obvious celestial bodies lend their names to the first and second days of the week. But how were the remaining days assigned their planets?
