---
title: Django JavaScript Integration - Book Review
date: '2011-04-10'
aliases: [/2011/04/10/django-javascript-integration-ajax-and-jquery/]
categories:
  - Python
  - Book Reviews
tags:
  - Python
---

![](/images/django-javascript-book.jpeg#fig-right "Front cover")

[Django JavaScript Integration: AJAX and jQuery](http://www.packtpub.com/django-javascript-integration-ajax-and-jquery/book) is a book about the building of Ajax-enabled web applications using Django and jQuery.  Django has rapidly shot to fame as the most popular web development framework for the Python programming language.  Similarly, jQuery has taken the Javascript world by storm as a client-side Javascript framework making the development of sophisticated browser based clients both easier and even more pleasurable than using Javascript alone.  The strapline to this book is: “Develop AJAX applications using Django and jQuery” and I would suggest that this describes the aim of the book more accurately than its title.

There’s a wealth of both online and dead-tree texts covering Django and jQuery, however by comparison, there’s far less information covering the integration of both technologies so the arrival of this book is timely.  I’m also always happy to see new books aimed at the more experienced Python programmer in a time when the rapid (and very welcome) growth in the adoption of Python has led to the recent publication of a large number of beginners’ books.

To get the most out of this book, a knowledge of Python is expected and a working knowledge of Javascript and Django highly recommended.  The author also makes occasional (and perhaps inevitable) comparisons between Javascript and the Java language in the first couple of chapters, however a working knowledge of Java is definitely not needed.

The first chapter covers Python and Javascript.  As a Django/jQuery developer you’ll be using both languages and the author provides some interesting comparisons between the two.  The author is also quite candid and realistic about the weaknesses of Javascript and its cross-browser incompatibilities whilst carefully highlighting its strengths:  “If you can figure out why Python is a good language, you can figure out why JavaScript is a good language.”

The second chapter gets stuck into the basics of jQuery and the constructs which simplify the implementation of Ajax.  The third chapter then dives into Django with a tour of Django validation and a detailed discussion of validation in general.  The remainder of the book builds a reasonably large web application with each chapter pulling together a good number of disparate features you’d want to provide in any self-respecting Web 2.0 application.  Autocompletion, form validation, server-side validation, client-side and server-side search and login handling are all described and integrated into the application.  Even the creation of a “favicon.ico” is mentioned to put a company logo on your users’ web browser tabs and make them look distinctive.

It quickly became apparent that this book  is not a regurgitation of “the same old stuff”, rather it makes the effort not only to show you what to do, but also to discuss why you do something in a particular way and how you can improve on it, leaving the reader with a deeper understanding.  For example, the book is quite happy to extend the provided Django classes where they fall short, and show validation of more unusual types such as GPS coordinates not natively supported by Django.  Another example is the book’s excellent treatment of validation discussing cultural awareness and the suggestion that a “less is more” approach to validation can sometimes make sense.

Apart from a couple of typos here and there (which are possibly restricted to my electronic copy), a minor annoyance is what I felt to be a rather unorthodox Javascript formatting style.  For example:

```javascript
set: function(newValue)
   {
   var value = parseInt(newValue.toString());
   if (isNaN(value))
       {
       return false;
       }
   else
       {
       field = value;
       return true;
       }
   }
```

I acknowledge that you can never please everyone with your coding style and layout!

The book stops short of helping you organise the inevitable growing mass of Javascript code, a difficult but increasingly important topic.  A little information around the modularisation of Javascript files or strategies and libraries for implementing MVC in client side code would have gone a long way.  Another aspect of the book which is notably glossed over is the topic of testing.  Testing can be hard, and testing web applications can be very hard, particularly those which rely on a lot of Javascript.  Admittedly this isn’t a book about testing, but implementing tests is a very important part of a developer’s life and a section or chapter setting the reader on the right path would have been welcome.

There are several parts of the book which deserve a special mention, however Chapter 11 particularly stands out.  The topic of usability is one often brushed over in technical books in favour of delivering more how-to’s and code examples.  The author devoted an entire chapter to usability, a chapter which I can only hope the authors of many web applications I’ve used might one day read.

I find it hard to characterise the author’s style of writing but I’d probably describe it as intellectual bordering on philosophical with a colourful vocabulary, a style which I enjoy but might not be to everyone’s taste.  An amusing example of the intellectual nature of the book can be found in Chapter 2: “Prototypal inheritance is more like the evolutionary picture of single-celled organisms that can mutate, reproduce asexually by cell division, and pass on (accumulated) mutations when they divide.”  I actually found this an interesting and useful analogy however it’s probably a little hard to relate to unless you remember your school biology!

In summary, I like this book.  I like the the fact that it’s filled with gems of information you won’t easily find online.  I like the colourful language and the interesting discussion around the concepts the author is conveying.  Most importantly, this book is written by someone who has clearly developed real web applications.  If you’re someone merely looking to get cracking on a project using Django and jQuery in the shortest time possible, then this book might disappoint.  But then again, the online tutorials and references are there to get you started and this book can take over where they leave off.

Finally, the author strikes me as someone both interesting and accomplished and I look forward to reading other books he might have in the works.
