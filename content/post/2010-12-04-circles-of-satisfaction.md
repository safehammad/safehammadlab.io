---
title: The Circles of Satisfaction
date: '2010-12-04'
aliases: [/2010/12/04/the-circles-of-satisfaction/]
categories:
  - Work
tags:
  - Work
---

What motivates us to get a job?  The instinctive answer for many might be: "To earn money!" And what motivates us to stay in that job? A member of the developed world might then pause for thought and mutter enlightened words such as "fulfilment" and "sense of worth".

I've been thinking for some time about the factors that contribute to job satisfaction. More specifically, I'm interested in why people stay in their jobs and at what point they decide to move on. There are seemingly countless factors to consider, including money earned, working hours, length and ease of commute, boss, colleagues, being challenged etc. Each of these factors contributes variably to whether you stay or go.

In an attempt to better understand these factors and their influence on decision making, I turned to thinking about [Maslow's Hierarchy of Needs](https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs) which proposes a theory of human motivations and behaviour based on needs. Translating this to the workplace, I found that there are generally three categories of need:

* **Remuneration.** Earning enough money to pay the bills and earning what you feel you are worth, whether it be with salary, commissions or payment in kind, for example, lavish trips or a generous expense account.
* **Enjoyment.** Getting a kick out of doing what you do, for example, by being challenged either mentally or physically. Getting to help others. Enjoying the company and camaraderie of colleagues. Getting on with your boss.
* **Convenience.**  Sensible commute. Sensible working hours that don't interfere with your non-work pursuits or family life.

These categories are best considered equally important to each other rather than hierarchical. Enter The *Circles of Satisfaction*:

![](/images/job-satisfaction.png "The Circles of Satisfaction")

The formula is simple. Add up the number of circles that apply to you:

* If you score 3 out of 3, you’re very satisfied in your job and very lucky.
* If you score 2 out of 3, you’re generally satisfied in your job and happy to stick with it.
* If you score 1 out of 3, you’re probably looking for a job elsewhere.

Of course, if you score 0, you’re in real trouble!
