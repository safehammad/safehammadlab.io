---
title: Quad Crystals - Generative Art No. 2
date: '2024-10-07'
categories:
  - Generative Art
  - Software
tags:
  - Clojure
---

![](/images/quad-crystals.png "Quad Crystals")

The second piece of artwork in my [Generative Art Practice]({{< relref "2024-09-20-generative-art-practice.md" >}}) series is *Quad Cystals*. I began by drawing a set of quadrilaterals radiating out from the centre, and in fact, the piece started off life being called *Quad Ray*, but as the piece developed, the quads look less like rays emanating from the centre, and more like crystals.

Of interest, I've use two different sources of (pseudo)random numbers for this piece...

Firstly, I've employed the [Halton Sequence](https://en.wikipedia.org/wiki/Halton_sequence) which I also used for [Crimson Sun]({{< relref "2024-09-27-crimson-sun.md" >}}). In this instance, it's to generation the locations and dimensions of the quads, and from those, to derive the basic colour of each quad.

Secondly, I've sourced random numbers from a Gaussian distribution, also known as a [normal distribution](https://en.wikipedia.org/wiki/Normal_distribution). Random numbers drawn from the normal distribution, when plotted on a graph, follow a so-called and familiar Bell Curve whereby numbers drawn are most likely gathered around the centre of the graph, and the likelihood of a number being drawn tails off the further away it is from the centre:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Standard_deviation_diagram.svg/1920px-Standard_deviation_diagram.svg.png "Bell Curve")

The normal distribution is commonly found in nature. For example, if you were to plot people's heights, or their blood pressures, or their IQ, you will find that they follow a normal distribution.

The [Quil](https://github.com/quil/quil) library which I use to create my artwork has a convenient function `(quil.core/random-gaussian)` for generating random numbers from a normal distribution. Each time it is called, it will return a new random number, and plotting a large set of numbers will likely produce a graph centred around the number 0 with the majority of numbers lying between -1 and 1. This piece uses these random numbers to vary the colour of the quads, and to "jitter" the quads i.e. to redraw each quad with a slight variation in the coordinates. The jitter is used to soften the quads to make them look a little natural and perhaps even hand drawn.

Code used to create all artwork in the Generative Art Practice series can be found in this [Github repository](https://github.com/safehammad/generative-art-practice).
