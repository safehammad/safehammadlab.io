---
title: Square Wave - Generative Art No. 3
date: '2024-10-19'
categories:
  - Generative Art
  - Software
tags:
  - Clojure
---

![](/images/square-wave.png "Square Wave")

The third piece in my [Generative Art Practice]({{< relref "2024-09-20-generative-art-practice.md" >}}) series is *Square Wave*.

*Square Wave* was born after I (finally!) read the book [Metamagical Themas](https://en.wikipedia.org/wiki/Metamagical_Themas), an assorted assembly of articles written for the magazine [Scientific American](https://en.wikipedia.org/wiki/Scientific_American) by the cognitive scientist, computer scientist, and polymath, [Douglas Hofstadter](https://en.wikipedia.org/wiki/Douglas_Hofstadter). One of his articles from July 1983 is titled "Parquet Deformations: A Subtle, Intricate Art Form" where he asks about the difference between music and visual art and discusses the idea of trying to capture the essence of the musical experience in visual form. As Hofstadter explains, a parquet is a regular mosaic made of inlaid wood, and the Parquet Deformations he refers to are artwork displaying a gradual change and progression in the shape of these mosaics over time. I believe the examples he offers can quite clearly be placed in the camp of generative art. One example I singled out is called "I at the Center" by David Oleson:

![](/images/i-at-the-center-transparent.png "I at the Center")

You'll notice in this image that the proverbial letter "I" at the centre of the image is perfect but becomes more deformed as your eye moves toward the edges, where each edge has its own unique deformation. I considered how I might replicate this in code and logically felt I should start with the "I" at the centre and work outward. As you can see from the resulting piece, I quickly and happily deviated from my task again and again, both in form and in colour, until I was happy with the result. Iterative development and embracing unexpected deviation in each iteration is a common theme in generative art practice, and one that I plan to discuss in a future post.

As a final note, I can highly recommend the book [Metamagical Themas](https://en.wikipedia.org/wiki/Metamagical_Themas). While it couldn't by any stretch be described as an easy read, each article is relatively self-contained which makes the book quite digestible article by article. Three of the articles discuss the [Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language)) programming language of which [Clojure](https://en.wikipedia.org/wiki/Clojure), the language used to create *Square Wave*, is a dialect. To me, that makes this piece particularly metamagical!

Code used to create all artwork in the Generative Art Practice series can be found in this [Github repository](https://github.com/safehammad/generative-art-practice).
