---
title: About
---

This is the personal blog of _Safe Hammad_. All content represents my views alone.

Links:

* [Github](https://github.com/safehammad)
* [LinkedIn](https://www.linkedin.com/in/safehammad/)
* [Twitter](https://twitter.com/safehammad)
